package jdkImpl;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Subscriber implements Observer {
    int id;
    Subscriber(int id){
        this.id=id;
    }
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("ID"+id+":我收到新一期报刊啦！"+"内容是："+arg.toString());
    }
}
