package jdkImpl;

import java.util.Observable;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Publisher extends Observable {
    public void publish(String content){
        System.out.println("（jdk内置）新报刊发布啦！即将发布到各位邮箱！");
        setChanged();
        notifyObservers(content);
    }
}
