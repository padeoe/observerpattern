package domino;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Bottle extends Observable implements Observer{
    String name;
    Bottle(String name){
        this.name=name;
    }
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Bottle "+name+" said:I'm crashed by "+arg);
        setChanged();
        notifyObservers(name);
    }
}
