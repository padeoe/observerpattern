package domino;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Start {
    /**
     * 模拟了一个连环订阅发布的过程
     * 老鼠发出了叫声，
     * 猫听见后去追，撞翻了瓶子
     * 瓶子被撞翻，吵醒了主人
     * @param args
     */
    public static void main(String args[]){
        Mouse mouse=new Mouse("zhi~zhi~");
        Cat cat1=new Cat("[cat] miaomiao");
        Cat cat2=new Cat("[cat] huahua");
        Bottle bottle1=new Bottle("[bottle]beer bottle");
        Bottle bottle2=new Bottle("[bottle]coco bottle");
        Master master=new Master("[master]John");
        mouse.addObserver(cat1);
        cat1.addObserver(bottle1);
        bottle1.addObserver(master);
        mouse.runAndMakeNoise();
    }
}
