package domino;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Master implements Observer {
    String name;
    Master(String name){
        this.name=name;
    }
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Master "+name+" said: I'm waken up by "+arg);
    }
}
