package domino;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Cat extends Observable implements Observer{
    String name;
    Cat(String name){
        this.name=name;
    }
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Cat "+name+" said: I heard mouse's sound:"+arg.toString()+",I need to catch him!");
        setChanged();
        notifyObservers(name);
    }

}
