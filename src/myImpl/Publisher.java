package myImpl;

import java.util.ArrayList;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Publisher {
    ArrayList <MyObserver> observerList=new ArrayList<>();
    public void addObserver(MyObserver myObserver){
        observerList.add(myObserver);
    }

    public void publish(String content){
        System.out.println("出版社：新一期杂志刚刚发布，即将发送到各位邮箱");
        notifyObserver(content);
    }
    public void notifyObserver(String content){
        for(MyObserver myObserver:observerList){
            myObserver.update(this,content);
        }
    }

}
