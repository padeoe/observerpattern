package myImpl;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Start {
    /**
     * 完整实现的观察者模式
     * @param args
     */
    public static void main(String args[]){
        Publisher publisher=new Publisher();
        Subscriber subscriber1=new Subscriber(1);
        Subscriber subscriber2=new Subscriber(2);
        Subscriber subscriber3=new Subscriber(3);
        publisher.addObserver(subscriber1);
        publisher.addObserver(subscriber2);
        publisher.addObserver(subscriber3);
        publisher.publish("第一刊正文...");
    }
}
