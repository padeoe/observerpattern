package myImpl;

/**
 * Created by padeoe on 2016/3/7.
 */
public interface MyObserver {
    public void update(Publisher publisher,Object arg);
}
