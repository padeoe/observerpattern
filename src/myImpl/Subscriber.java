package myImpl;

/**
 * Created by padeoe on 2016/3/7.
 */
public class Subscriber implements MyObserver {
    int id;
    MyObserver myObserver;
    Subscriber(int id){
        this.id=id;
    }
    @Override
    public void update(Publisher publisher,Object arg) {
        System.out.println("ID"+id+":我收到新一期报刊啦！");
    }
}
